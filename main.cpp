#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_CHARACTERS 1000

//FUNCTIONS
void n(char *newText) {
	//Otvaranie suboru
	FILE *fr;
	if((fr = fopen("sifra.txt", "r")) == NULL) {
		printf("Chyba: Spravu sa nepodarilo nacitat\n");
		return;
	}
	
	int i = 0;
	char newC;
	while(i < MAX_CHARACTERS) {		
		newC = getc(fr);
		if(newC != EOF) {	
			newText[i] = newC;
			i++;
		} else break; 
		
	}
	//Zatvorenie s�boru
	if(fclose(fr) == EOF) {
		printf("Chyba: Subor sa nepodarilo zatvorit\n");
		return;
	}
}

void v(char originalMessage[]) {
	if(strlen(originalMessage) == 0) {
		printf("Chyba: Sprava nieje nacitana!\n");
		return; 
	} else {
		printf("%s\n", originalMessage);
	} 
}

void s(char editedText[]) {
		if(strlen(editedText) == 0) {
			printf("Chyba: Upravena sprava nieje k dispozicii!\n");
			return; 
		} else {
			int i=0;
			while(editedText[i] != '\0') {
				printf("%c", editedText[i]);		
				i++;
			}
			printf("\n");
		} 
}

void u(char originalText[], char *editedText) {
	if(strlen(originalText) == 0) {
		printf("Chyba: Sprava nieje nacitana!\n");
		return; 
	} else {
		int newArrayIndex = 0;
		int i = 0;
		while(originalText[i] != '\0') {		
				if(originalText[i] >= 'A' && originalText[i] <= 'Z') {
					editedText[newArrayIndex] = originalText[i];		
					newArrayIndex++;
				} else if(originalText[i] >= 'a' && originalText[i] <= 'z') {
					char bigChar = originalText[i] - 32;		
					editedText[newArrayIndex] = bigChar;	
					newArrayIndex++;
				}			
			i++;
		}
	} 
}

void d(char text[]) {
	if(strlen(text) == 0) {
			printf("Chyba: Sprava nieje nacitana!\n");
			return; 
	} else {
		int length = 0;
		scanf("%d", &length);
		char savedWord[MAX_CHARACTERS];
		int sizeOfWord = 0;
		for(int i=0; i <= strlen(text); i++) {
			if(text[i] == ' ' || text[i] == '\n' || text[i] == '\0') {
				if(sizeOfWord == length) {
					printf("%s\n", savedWord);
					sizeOfWord = 0;
					memset(savedWord, 0, sizeof(savedWord));
				} else  {
					sizeOfWord = 0;
					memset(savedWord, 0, sizeof(savedWord));
				}
			} else {
				savedWord[sizeOfWord] = text[i];
				sizeOfWord++;	 
			}		
		}
	}
}

void h(char text[]) {
	if(strlen(text) == 0) {
		printf("Nie je k dispozicii upravena sprava\n");
		return; 
	} else {
		printf("\n");
		float characters[26] = {0};
		int textLenght = strlen(text);
		for(int i=0; i<textLenght; i++) {
			characters[text[i]-65]++;
		}
		
		for(int i=0; i<26; i++) {
			characters[i] = (characters[i] / textLenght)*10;
		}
		float max = characters[0];
		for(int i = 1; i < 26; i++){
        	if (characters[i] > max) max = characters[i] + 1; 
		} 
		for(float row=max;row>0;row--) {
			for(int col = 0; col < 26;col++) {
				if(characters[col] > 0 && characters[col] >= (row)) printf("*");
				else printf(" ");
			}
			printf("\n");
		}
		for(int i=0; i<26;i++) {
			printf("%c", 65 + i);
		}
		printf("\n");	
	}
}

void c(char text[]) {
	if(strlen(text) == 0) {
		printf("Nie je k dispozicii upravena sprava\n");
		return; 
	} else {
		int n = 0;
		while(n<=0 || n>25) {			
			scanf("%d", &n);
		}
		int textLength = strlen(text);
		for(int i = 0; i< textLength;i++) {
			char newChar = text[i] - n;
			if(newChar < 'A') {
				int diference = 'A' - newChar;
				printf("%c", 91 - diference);
			} else printf("%c", newChar);
		}
		printf("\n");	
	}
}
int main(int argc, char** argv) {
	char upravenyText[MAX_CHARACTERS] = {};
	char povodnyText[MAX_CHARACTERS] = {};
	char *pPovodnyText;
	char *pUpravenyText;
	pPovodnyText = povodnyText;
	pUpravenyText = upravenyText;
	
	char vstup;
	while(vstup != 'k') {
		scanf("%c", &vstup);
		switch(vstup) {
			case 'n': 
				n(pPovodnyText);
				break;
			case 'v': 
				v(pPovodnyText);
				break;
			case 'u':
				u(povodnyText, pUpravenyText);
				break;
			case 's':
				s(upravenyText);
				break;
			case 'd':
				d(pPovodnyText);
				break;
			case 'h':
				h(pUpravenyText);
				break; 
			case 'c':
				c(pUpravenyText);
				break;
		}
	}
	return 0;
}
